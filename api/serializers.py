from rest_framework import serializers
from core.models import Entry, Folder


class SecretSerializer(serializers.Field):
    """Provides a method of allowing updates of the secret without revealing it."""

    def to_representation(self, obj):
        return '***************'

    def to_internal_value(self, data):
        print('DATA = {}'.format(data))
        if data == '***************':
            return None
        return data


class EntrySerializer(serializers.ModelSerializer):
    """Serializes the Entry model for use with DRF's list views."""

    class Meta:
        model = Entry
        fields = ('id', 'name', 'folder', 'url', 'login', 'description')


class EntryRetrieveSerializer(serializers.ModelSerializer):
    """Serializes the Entry model for use with DRF's retrieve views."""
    secret = serializers.SerializerMethodField()

    class Meta:
        model = Entry
        fields = ('id', 'name', 'folder', 'url', 'login', 'description', 'secret')

    def get_secret(self, obj):
        return obj.decrypt()


class EntryCreateEditSerializer(serializers.ModelSerializer):
    """Serializes the Entry model for use with DRF's create and update views."""
    secret = SecretSerializer()

    class Meta:
        model = Entry
        fields = ('id', 'name', 'folder', 'url', 'login', 'description', 'secret')


class FolderSerializer(serializers.ModelSerializer):
    """Serializes the Folder model for use with DRF's list views."""
    entries = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='entry-detail',
    )

    class Meta:
        model = Folder
        fields = ('id', 'name', 'parent', 'entries')


class FolderDetailSerializer(serializers.ModelSerializer):
    """Serializes the Folder model for use with DRF's detail views."""
    entries = EntrySerializer(
        many=True,
        read_only=True,
    )

    class Meta:
        model = Folder
        fields = ('id', 'name', 'parent', 'entries')
