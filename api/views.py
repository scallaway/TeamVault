from rest_framework import status, viewsets
from rest_framework.response import Response
from api.serializers import (EntryCreateEditSerializer, EntryRetrieveSerializer, EntrySerializer,
                             FolderDetailSerializer, FolderSerializer)
from core.models import Entry, Folder


class FolderViewset(viewsets.ModelViewSet):
    """API endpoint that allows folders to be viewed and edited."""
    queryset = Folder.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return FolderDetailSerializer
        else:
            return FolderSerializer


class EntryViewset(viewsets.ModelViewSet):
    """API endpoint that allows entries to be viewed and edited."""
    queryset = Entry.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return EntryRetrieveSerializer
        elif self.action in ('create', 'update', 'partial_update'):
            return EntryCreateEditSerializer
        else:
            return EntrySerializer

    def perform_update(self, serializer):
        """Update the Entry, ensuring that the masked secret isn't saved."""
        print('***GOT HERE***')
        if serializer.is_valid():
            print('VALIDATED_DATA = {}'.format(serializer.validated_data['secret']))
            if serializer.validated_data['secret'] is None:
                entry = self.get_object()
                serializer.validated_data['secret'] = entry.decrypt()
                serializer.save()

            else:
                serializer.save()
                return Response({'status': 'secret updated'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
