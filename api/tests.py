import json
from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status
from api.serializers import EntrySerializer, EntryRetrieveSerializer, FolderDetailSerializer, FolderSerializer
from core.models import Entry, Folder


client = Client()


class GetFoldersTest(TestCase):
    """Test module for GET folders API."""

    def setUp(self):
        self.root = Folder.objects.create(name='root')
        test1 = Folder.objects.create(name='Cloud', parent=self.root)
        Folder.objects.create(name='Databases', parent=self.root)
        Folder.objects.create(name='AWS', parent=test1)

    def test_get_all_folders(self):
        """Verify we can get all folders in the proper format and receive an HTTP 200."""
        response = client.get(reverse('folder-list'))
        folders = Folder.objects.all()
        serializer = FolderSerializer(folders, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_folder(self):
        """Verify we can get a single folder in the proper format and receive an HTTP 200."""
        response = client.get(reverse('folder-detail', kwargs={'pk': self.root.id}))
        folder = Folder.objects.get(id=self.root.id)
        serializer = FolderDetailSerializer(folder)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_folder(self):
        """Verify we get a proper 404 error when trying to get an invalid folder."""
        response = client.get(reverse('folder-detail', kwargs={'pk': '99'}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateUpdateFoldersTest(TestCase):
    """Test module for POST, PUT, and DELETE folders API."""

    def setUp(self):
        self.root = Folder.objects.create(name='root')
        self.valid_payload = {
            'name': 'Databases',
            'parent': 1,
        }

        self.invalid_payload = {
            'name': '',
            'parent': 1,
        }

    def test_create_valid_folder(self):
        """Verify we can create a new folder and get an HTTP 201."""
        old_count = Folder.objects.count()
        response = client.post(
            reverse('folder-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json',
        )
        new_count = Folder.objects.count()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(old_count+1, new_count)

    def test_create_invalid_folder(self):
        """Verify we cannot create a new folder with bad data."""
        old_count = Folder.objects.count()
        response = client.post(
            reverse('folder-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json',
        )
        new_count = Folder.objects.count()
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(old_count, new_count)

    def test_update_folder(self):
        """Verify that we can update a folder."""
        child = Folder.objects.create(name='Child', parent=self.root)
        response = client.put(
            reverse('folder-detail', kwargs={'pk': child.id}),
            data=json.dumps({'name': 'Changed'}),
            content_type='application/json',
        )
        child = Folder.objects.get(id=child.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(child.name, 'Changed')

    def test_invalid_update(self):
        """Verify we cannot update a folder with bad data."""
        """Verify that we can update a folder."""
        child = Folder.objects.create(name='Child', parent=self.root)
        response = client.put(
            reverse('folder-detail', kwargs={'pk': child.id}),
            data=json.dumps({'name': ''}),
            content_type='application/json',
        )
        child = Folder.objects.get(id=child.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(child.name, 'Child')

    def test_delete_folder(self):
        """Verify that we can delete a folder."""
        child = Folder.objects.create(name='Child', parent=self.root)
        old_count = Folder.objects.count()
        delete_response = client.delete(
            reverse('folder-detail', kwargs={'pk': child.id}),
        )
        new_count = Folder.objects.count()
        self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(old_count-1, new_count)
        test_response = client.get(reverse('folder-detail', kwargs={'pk': child.id}))
        self.assertEqual(test_response.status_code, status.HTTP_404_NOT_FOUND)


class GetEntryTest(TestCase):

    def setUp(self):
        self.folder = Folder.objects.create(name='Root')
        self.testentry = Entry.objects.create(name='Test', folder=self.folder, secret='test')

    def test_get_entry(self):
        response = client.get(reverse('entry-detail', kwargs={'pk': self.testentry.id}))
        item = Entry.objects.get(id=self.testentry.id)
        serializer = EntryRetrieveSerializer(item)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
