from django.urls import include, path
from rest_framework import routers
from api import views


router = routers.DefaultRouter(trailing_slash=False)
router.register('folder', views.FolderViewset)
router.register('entry', views.EntryViewset)

urlpatterns = [
    path('', include(router.urls))
]
