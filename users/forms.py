from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from users.models import TVUser


class TVUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = TVUser
        fields = ('username', 'email')


class TVUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model = TVUser
        fields = UserChangeForm.Meta.fields
