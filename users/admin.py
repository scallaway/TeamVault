from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.forms import TVUserChangeForm, TVUserCreationForm
from users.models import TVUser


class TVUserAdmin(UserAdmin):
    model = TVUser
    add_form = TVUserCreationForm
    form = TVUserChangeForm


admin.site.register(TVUser, TVUserAdmin)
