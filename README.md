# TeamVault
A team-based password manager with RBAC and usability in mind.

[![Build Status][travis-image]][travis-url]
[![Commits][commit-image]][commit-url]

After evaluating and using numerous FOSS password managers, I decided to create my own, as none of them
had all of the features I desired: solid encryption, folders, groups/RBAC, and an API which *also* 
was subject to RBAC. 

Note that the name of the project will be changing, as I've found that another TeamVault exists. 

## Installation

_Coming soon._

## Usage example

_Coming soon._

## Development setup

I'm currently developing on Linux, so can only provide an example for Linux/OS X.

```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Release History

_No releases yet._

## Meta

### Contributors

Sean Callaway – [@smcallaway](https://twitter.com/smcallaway) – seancallaway@gmail.com

### Licensing

Distributed under the GPLv2 license. See ``LICENSE`` for more information.

[https://github.com/seancallaway/TeamVault](https://github.com/seancallaway/TeamVault)

## Contributing

1. Fork it (<https://github.com/seancallaway/TeamVault/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[wiki]: https://github.com/seancallaway/TeamVault/wiki
[license-image]: https://img.shields.io/github/license/seancallaway/TeamVault.svg
[license-url]: https://github.com/seancallaway/TeamVault/blob/master/LICENSE
[commit-image]: https://img.shields.io/github/last-commit/seancallaway/TeamVault.svg
[commit-url]: https://github.com/seancallaway/TeamVault/commits/master
[travis-image]: https://img.shields.io/travis/seancallaway/TeamVault/master.svg
[travis-url]: https://travis-ci.org/seancallaway/TeamVault
