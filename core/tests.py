from django.test import TestCase
from core.models import Entry, Folder


class FolderModelTest(TestCase):
    """Tests the Folder Model."""

    def test_string_representation(self):
        """Verify the string representation includes the name."""
        folder = Folder(name='Test Folder')
        self.assertIn(folder.name, str(folder))


class EntryModelTest(TestCase):
    """Tests the Entry Model."""

    def setUp(self):
        self.folder = Folder.objects.create(name='Root')
        self.testentry = Entry.objects.create(name='Test', folder=self.folder)

    def test_string_representation(self):
        """Verify the string representation includes the name."""
        entry = Entry(name='Test Entry')
        self.assertIn(entry.name, str(entry))
    
    def test_encryption(self):
        """Verify that entries will encrypt data."""
        words = "Shh! It's a secret."
        self.testentry.secret = words
        self.assertNotEqual(words, self.testentry.secret)

    def test_decryption(self):
        """Verify that entries will decrypt data."""
        words = "I drink and I know things."
        self.testentry.secret = words
        self.testentry.save()
        new_words = self.testentry.decrypt()
        self.assertEqual(words, new_words)
