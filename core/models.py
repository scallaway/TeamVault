from base64 import b64decode, b64encode
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad
from django.conf import settings
from django.db import models


def generate_iv_value():
    """Generates an initialization vector for entries.
    
    This function is used as the default value for Entry.iv
    """
    return b64encode(get_random_bytes(16)).decode('utf-8')


class Folder(models.Model):
    """Represents a folder in which entries can be stored."""
    name = models.CharField(max_length=50, blank=False, db_index=True)
    parent = models.ForeignKey('self', unique=False, null=True, default=None, related_name='children', on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'folder'
        verbose_name_plural = 'folders'

    def __str__(self):
        return '<Folder {}>'.format(self.name)


class Entry(models.Model):
    """Represents a secret stored in TeamVault."""
    name = models.CharField(max_length=120, blank=False, db_index=True)
    folder = models.ForeignKey(Folder, related_name='entries', on_delete=models.CASCADE)
    url = models.URLField(null=True, blank=True)
    login = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    _iv = models.CharField(max_length=24, blank=True, default=generate_iv_value)
    _secret = models.CharField(max_length=512, blank=True)

    class Meta:
        verbose_name = 'entry'
        verbose_name_plural = 'entries'
    
    def __str__(self):
        return '<Entry {}>'.format(self.name)

    def set_iv(self, iv):
        self._iv = b64encode(iv).decode('utf-8')
    
    def get_iv(self):
        return b64decode(self._iv)
    
    iv = property(get_iv, set_iv)

    def set_secret(self, secret):
        enc_suite = AES.new(settings.ENC_KEY, AES.MODE_CBC, iv=self.iv)
        data = pad(secret.encode(), enc_suite.block_size)
        cipher_text = enc_suite.encrypt(data)
        self._secret = b64encode(cipher_text).decode('utf-8')

    def get_secret(self):
        return b64decode(self._secret)

    secret = property(get_secret, set_secret)

    def decrypt(self):
        """Returns the decrypted version of the secret."""
        enc_suite = AES.new(settings.ENC_KEY, AES.MODE_CBC, iv=self.iv)
        plaintext = unpad(enc_suite.decrypt(self.secret), enc_suite.block_size)
        return plaintext.decode('utf-8')
