import core.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=120)),
                ('url', models.URLField(blank=True, null=True)),
                ('login', models.CharField(blank=True, max_length=200, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('_iv', models.CharField(blank=True, default=core.models.generate_iv_value, max_length=24)),
                ('_secret', models.CharField(blank=True, max_length=512)),
            ],
            options={
                'verbose_name': 'entry',
                'verbose_name_plural': 'entries',
            },
        ),
        migrations.CreateModel(
            name='Folder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=50)),
                ('parent', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='core.Folder')),
            ],
            options={
                'verbose_name': 'folder',
                'verbose_name_plural': 'folders',
            },
        ),
        migrations.AddField(
            model_name='entry',
            name='folder',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='entries', to='core.Folder'),
        ),
    ]
